# Proyecto de evaluación de **Gravitee**

Para referencia de la herramienta, consultar [Gravitee](https://gravitee.io/)

El proyecto está organizado de la siguiente forma:

1. **Carpeta gravitee**: Archivos para el docker-compose que levanta el ecosistema de gravitee. 
    
    Se ejecuta con ``docker-compose up``
    
    Si todo levanta con éxito, podran acceder a las siguientes ligas (son rutas locales):

    * [Definición del servicio bajo el estandar open-api](http://localhost:8080/test/api-docs)
    * [Servicio expuesto](http://localhost:8080/test/greeting)
    * [Consola de gravitee](http://localhost:8002/)

    <img src="/images/managment.png"  width="318" height="243">
    <img src="/images/service-openapi.png"  width="311" height="283">
    <img src="/images/service.png"  width="333" height="110">

    Esta compuesto de los siguientes contenedores:
    
    * Elasticsearch: Motor de analítica y análisis para todos los tipos de datos, incluidos textuales, numéricos, 
    geoespaciales, estructurados y desestructurados.
    * Mongodb: Base de datos no relacional.
    * Gateway: Su objetivo es ser un puente entre las aplicaciones y los proveedores de identidad para autenticar, 
    autorizar y obtener información sobre las cuentas de los usuarios.
    * Management-ui: Front de la aplicación, accesible via web. 
    * Management-api: Es una solución de administración para API que ayuda a controlar con quién, cuándo y cómo 
    los usuarios acceden a sus API. 
    * Rest service: Servicio de prueba para el gravitee, publicado en el puerto 8080.
    
2.  **Carpeta example-test**: Proyecto básico con spring boot para exponer un servicio rest. Para efectos de la prueba 
    no hay que generar nada en esta parte pues la imagen docker con el proyecto ya está publicada en el 
    [repositorio público](https://hub.docker.com/r/ingaragon/test/tags)  
    Tambien incluye el ``Dockerfile`` muestra (muy básico) para generar un contenedor con un una
    app desarrollada con springboot.
  
    * Se expone el endpoint /test/greeting
