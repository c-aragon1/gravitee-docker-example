package com.example.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class App01Controller {

	private static final String template = "Hello, %s!";
	
	private final AtomicLong counter01 = new AtomicLong();
	
	private final AtomicLong counter02 = new AtomicLong();

	@GetMapping("service01")
	public Greeting service01(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter01.incrementAndGet(), String.format(template, name));
	}
	
	@GetMapping("service02")
	public Greeting service02(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter02.incrementAndGet(), String.format(template, name));
	}
}
